# Weather ML tutorial

A small project that showcases the capabilities of ML-based blending of weather forecasts. The basic forecasts are provided by GFS and CMC.

After cloning the repo, run

`pip install requirements.txt`

The only external package needed is GRIB API which is a requirement for pygrib. The details can be found here: https://confluence.ecmwf.int//display/GRIB/Home. 

External files for the tutorial are located here:

https://drive.google.com/drive/folders/1XMGCqFH3LaC_TbjLIex-b6NDVcq2cibu
